import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { getTodos, addTodo, editTodo, deleteTodo } from './todoListAPI';

const initialState = {
  todos: [],
  isLoading: false,
  isAddingTodo:false,
  firstsLoad: true,
};

export const fetchTodosAsync = createAsyncThunk(
  'todos/fetchTodos',
  async () => {
    const response = await getTodos();
    return response.data;
  }
);

export const addTodoAsync = createAsyncThunk(
  'todos/addTodo',
  async (todoTitle) => {
    const response = await addTodo(todoTitle);
    return response.data.body;
  }
);

export const todoListSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    editTodoA: (state , action )=>{
      let updatedTodos = []
      state.todos.forEach(element =>{
        if(element.id !== action.payload.id){
          updatedTodos.push(element)
        }else{
          updatedTodos.push(action.payload)
        }
      })

      state.todos = updatedTodos
    },
    deleteTodoA:(state, action) =>{
      state.todos = state.todos.filter((td)=> td.id !== action.payload )
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(addTodoAsync.pending, (state) => {
        state.isAddingTodo = true;
      })
      .addCase(addTodoAsync.fulfilled, (state, action) => {
        state.isAddingTodo = false;
        console.log(action)
        state.todos = [...state.todos, action.payload.todo]
      })
      .addCase(fetchTodosAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(fetchTodosAsync.fulfilled, (state, action) => {
        state.isLoading = false
        state.todos = [...action.payload]
      })
      
  },
});

export const selectTodos = (state) => state.todos.todos;
export const selectIsAddingTodo = (state) => state.todos.isAddingTodo;

export const { editTodoA, deleteTodoA } = todoListSlice.actions

export const editTodoAsync = (todo) => (dispatch) => {
    
    dispatch(editTodoA(todo))
    editTodo(todo)
  
};
export const deleteTodoAsync = (todoId) => (dispatch) => {
    
  dispatch(deleteTodoA(todoId))
  deleteTodo(todoId)

};

export default todoListSlice.reducer;
