FROM node:alpine AS builder

WORKDIR /home/node/app/

COPY . .

RUN npm install
RUN npm run build


FROM nginx:stable-alpine

COPY ./.nginx/config/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /home/node/app/build /usr/share/nginx/html

ENV NGINX_PORT=5000

EXPOSE 5000


