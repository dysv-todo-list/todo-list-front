import React, { useState, useEffect, useRef } from 'react';
import { getTodos } from './todoListAPI'
import { useSelector, useDispatch } from 'react-redux';
import {
  selectTodos,
  selectIsAddingTodo,
  fetchTodosAsync,
  addTodoAsync,
  editTodoAsync,
  deleteTodoAsync,

} from './todoListSlice.js';
import {
  Container,
  Button,
  Icon,
  Checkbox,
  Input,
  Segment,
} from 'semantic-ui-react'

import './index.css'

const filterElements = [
  {
    text: 'All',
    iconName: 'filter',
    filter: (td)=>(true),
  },
  {
    text: 'Completed',
    iconName: 'check square outline',
    filter: (td)=>(td.is_completed === true),
  },
  {
    text: 'Incompleted',
    iconName: 'square outline',
    filter: (td)=>(td.is_completed === false),
  },
]


export function TodoList() {
  const [filterNumber, setFilter] = useState(0)
  const listTodos = useSelector(selectTodos);
  const dispatch = useDispatch();

  const firstLoad = useRef(true)

  useEffect(() => {
    if (firstLoad.current) {
      firstLoad.current = false
      dispatch(fetchTodosAsync())
    }
  })

  const handleChangeFilter =()=>{
    if(filterNumber>= filterElements.length - 1 ){
      setFilter(0)
      return
    }

    setFilter(filterNumber+ 1)
  }



  return (
    <div style={{ maxWidth: "500px", display: "inline-block", width: "100%" }}>
      <AddTodo isLoading={false} />
      <Filter filter={filterElements[filterNumber]} onChange={handleChangeFilter}/>
      <List todos={listTodos.filter(filterElements[filterNumber].filter)} />
    </div>
  );
}

function Filter({filter, onChange}) {
  return (
    <Button fluid onClick={onChange}>
      <Icon name={filter.iconName}/>
        {filter.text}
    </Button>
  )
}

function AddTodo({ }) {

  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsAddingTodo);
  const [inputTitle, setInputTitle] = useState("")

  useEffect(() => {
    if (!isLoading) {
      setInputTitle("")
    }
  }, [isLoading])

  const handleAdd = () => {
    getTodos()
    if (isLoading) {
      return
    }
    if (inputTitle.length === 0) {
      return
    }
    dispatch(addTodoAsync(inputTitle))
  }

  return (
    <Container>
      <Input
        fluid
        loading={isLoading}
        disabled={isLoading}
        action={{ icon: 'plus', onClick: handleAdd }}
        onChange={(e) => { setInputTitle(e.target.value) }}
        value={inputTitle}
        placeholder='new todo' />
    </Container>
  )
}

function List({ todos }) {
  return (
    <Container>
      {todos.map((element) => {
        return (
          <TodoItem key={element.id} todo={element} />
        )
      })
      }
    </Container>

  )
}

function TodoItem({ todo }) {
  const dispatch = useDispatch();
  const [isCompleted, setIsCompleted] = useState(todo.is_completed)
  const [inputTitle, setInputTitle] = useState(todo.title)
  const [isEditable, setIsEditable] = useState(false)

  const textStyle = isCompleted ? { textDecoration: 'line-through' } : {}
  const firstLoad = useRef(true)

  useEffect(() => {
    if (firstLoad.current) {
      firstLoad.current = false
      return
    }

    let updatedTodo = {
      ...todo,
      is_completed: isCompleted
    }

    dispatch(editTodoAsync(updatedTodo))

  }, [isCompleted])

  const handleCancelEdit = () => {
    setInputTitle(todo.title)
    setIsEditable(false)
  }

  const handleEditTitle = () => {

    if (inputTitle.length === 0) {
      setInputTitle(todo.title)
      setIsEditable(false)
      return
    }

    let updatedTodo = {
      ...todo,
      title: inputTitle
    }
    dispatch(editTodoAsync(updatedTodo))
    setIsEditable(false)
  }

  const handleDeleteTodo = () => {
    dispatch(deleteTodoAsync(todo.id))
  }



  return (
    <Segment style={{ padding: "14px 14px" }} vertical>
      <Checkbox checked={isCompleted} style={{ float: 'left' }} onChange={() => { setIsCompleted(!isCompleted) }} />
      {
        isEditable ?
          <Input autoFocus className="inputContainer" value={inputTitle} onChange={(e) => { setInputTitle(e.target.value) }} />
          :
          <span style={textStyle}>{todo.title}</span>
      }

      {
        isEditable ?
          <>
            <Icon style={{ float: 'right' }} link name='cancel' onClick={handleCancelEdit} />
            <Icon style={{ float: 'right' }} link name='check' onClick={handleEditTitle} />
          </>
          :
          <>
            <Icon style={{ float: 'right' }} link name='trash' onClick={handleDeleteTodo} />
            <Icon style={{ float: 'right' }} link name='edit' onClick={() => { setIsEditable(!isEditable) }} />
          </>

      }
    </Segment>
  )
}

