import React from 'react';
import { TodoList } from './features/todoList/TodoList';
import './App.css';
import {
  Header,
} from 'semantic-ui-react'

function App() {
  return (
    <div className="App">
      <Header as='h1' content='ToDo List' textAlign='center' />
      <TodoList/>
    </div>
  );
}

export default App;
