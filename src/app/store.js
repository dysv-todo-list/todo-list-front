import { configureStore } from '@reduxjs/toolkit';
import todosReducer from '../features/todoList/todoListSlice';

export const store = configureStore({
  reducer: {
    todos: todosReducer
  },
});
