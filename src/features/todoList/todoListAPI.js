import axios from "axios";

const TodosService = axios.create({
  baseURL: process.env.REACT_APP_TODOS_SERVICE
})

export function getTodos (){
  return TodosService.get("todos")
}

export function addTodo(todoTitle){
  return TodosService.post("todos", {title: todoTitle})
}

export function deleteTodo(todoId){
  return TodosService.delete(`todos/${todoId}`)
}

export function editTodo(todo){
  const {id,title, is_completed} = todo
  return TodosService.put(`todos/${id}`, {title, is_completed} )
}